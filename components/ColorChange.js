import {  StyleSheet, Text, View, Button } from 'react-native'
import React from 'react'

export default function ColorChange({color,onIncrease,onDecrease}) {
  return (
    <View>
      <Text>{color}</Text>
      <Button title={`${color} arttır`}  onPress={()=>onIncrease()}> 
      </Button>
      <Button title={`${color} azalt`}  onPress={()=>onDecrease()}>

</Button>
    </View>
  )
}

const styles = StyleSheet.create({})