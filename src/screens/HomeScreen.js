import { StyleSheet, Text, View, Button } from 'react-native';
import React from 'react';

export default function HomeScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Ana Ekran</Text> 
        <Button
        title="Renk Degistir"
        onPress={() => navigation.navigate('Renk Degistir')}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
